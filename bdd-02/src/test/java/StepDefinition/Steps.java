package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.SeleniumUtils;
import java.util.concurrent.TimeUnit;

public class Steps {

    ///////////////////////////////////
    //Espaniol

    @Dado("^Abro el Navegador y lanzo la aplicacion$")
    public void abro_el_navegador_y_lanzo_la_aplicacion() throws Throwable
    {
        System.out.println("This Step open the Firefox and launch the application.");
    }

    @Cuando("^Introduzco el usuario y la contrasenia$")
    public void introduzco_el_usuario_y_la_contrasenia() throws Throwable
    {
        System.out.println("This step enter the Username and Password on the login page.");
    }

    @Entonces("^Reseteo las credenciales$")
    public void Reseteo_las_credenciales() throws Throwable
    {
        System.out.println("This step click on the Reset button.");
    }

    ///////////////////////////////////

    WebDriver driver;

    @Given("^Open the Firefox and launch the application$")
    public void open_the_Firefox_and_launch_the_application() throws Throwable
    {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(SeleniumUtils.IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
    }

    @When("^Enter the Username and Password$")
    public void enter_the_Username_and_Password() throws Throwable
    {
        driver.findElement(By.name("uid")).sendKeys("username12");
        driver.findElement(By.name("password")).sendKeys("password12");
    }

    @Then("^Reset the credential$")
    public void Reset_the_credential() throws Throwable
    {
        driver.findElement(By.name("btnReset")).click();
    }


    @When("^Enter the Username \"(.*)\" and Password \"(.*)\"$")
    public void enter_the_Username_and_Password(String username,String password) throws Throwable
    {
        driver.findElement(By.name("uid")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
    }

}