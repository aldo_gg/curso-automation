#Line 1) In this line we write business functionality.
Feature: Funcionalidad de Reset en la pagina de login de la apliciacion

  #Line 2) In this line we write a scenario to test.
  Scenario: Se verifica el boton reset

    #Line 3) In this line we define the precondition.
    Given Abro el Navegador y lanzo la aplicacion

    #Line 4) In this line we define the action we need to perform.
    When Introduzco el usuario y la contrasenia

    #Line 5) In this line we define the expected outcome or result.
    Then Reseteo las credenciales