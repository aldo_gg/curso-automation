#Line 1) In this line we write business functionality.
Feature: Reset functionality on login page of Application

  #Line 2) In this line we write a scenario to test.
  Scenario: Verification of Reset button

    #Line 3) In this line we define the precondition.
    Given Open the Firefox and launch the application

    #Line 4) In this line we define the action we need to perform.
    When Enter the Username and Password

    #Line 5) In this line we define the expected outcome or result.
    Then Reset the credential