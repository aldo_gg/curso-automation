package com;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SeleniumUtils {

    public static final int IMPLICIT_WAIT_TIME=30;
    public static final int EXPLICIT_WAIT_TIME=30;

    public static void sleepSeconds(int s) {
        try {
            Thread.sleep(s*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is to waitClickable a webElement based on searchCriteria attribute
     *
     * @return
     */
    public static WebElement waitClickable(WebDriver driver, By searchCriteria) throws Exception {
        return waitClickable(driver, searchCriteria, EXPLICIT_WAIT_TIME);
    }

    /**
     * This method is to waitClickable a webElement based on searchCriteria attribute
     *
     * @return
     */
    public static WebElement waitClickable(WebDriver driver, By searchCriteria, int maxWaitTime) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, maxWaitTime);
            wait.until(ExpectedConditions.elementToBeClickable(searchCriteria));
            wait.until(ExpectedConditions.visibilityOfElementLocated(searchCriteria));
            driver.findElement(searchCriteria).isDisplayed();
            driver.findElement(searchCriteria).isEnabled();
            driver.findElement(searchCriteria).isSelected();

            List<WebElement> test= driver.findElements(searchCriteria);
            System.out.println("******** Total Found : "+ test.size()+ " by " + searchCriteria.toString());

            return driver.findElement(searchCriteria);
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("## ERROR - The Web UI Element : [" + searchCriteria.toString() + "] was not found in the page, message " + e.getMessage());
        } catch (Exception ex){
            throw new Exception("## ERROR - There was an error when finding the web element , error message "+ex.getMessage());
        }
    }

    /**
     * This method is to be visible a webElement based on searchCriteria attribute
     *
     * @return
     */
    public static WebElement wait(WebDriver driver, By searchCriteria) throws Exception {
        return wait(driver, searchCriteria, EXPLICIT_WAIT_TIME);
    }

    /**
     * This method is to be visible a webElement based on searchCriteria attribute
     *
     * @return
     */
    public static WebElement wait(WebDriver driver, By searchCriteria, int maxWaitTime) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, maxWaitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(searchCriteria));
            driver.findElement(searchCriteria).isDisplayed();
            driver.findElement(searchCriteria).isEnabled();
            driver.findElement(searchCriteria).isSelected();

            List<WebElement> test= driver.findElements(searchCriteria);
            System.out.println("******** Total Found : "+ test.size()+ " by " + searchCriteria.toString());

            return driver.findElement(searchCriteria);
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("## ERROR - The Web UI Element : [" + searchCriteria.toString() + "] was not found in the page, message " + e.getMessage());
        } catch (Exception ex){
            throw new Exception("## ERROR - There was an error when finding the web element , error message "+ex.getMessage());
        }
    }


}
