import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;


public class TestExplicitWait {
    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    String testUrl = "https://qa-02.vizix.cloud/console/";
    String usernameCorrect = "testroot";
    String passwordCorrect = "Bcb123456!";


    @Before
    public void setUp() {
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }

    @Test
    public void test1() throws Exception {
        driver.get(testUrl);
        driver.manage().window().setSize(new Dimension(1024, 768));

        By by;

        by = By.id("username");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).sendKeys(usernameCorrect);

        by = By.id("password");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).sendKeys(passwordCorrect);

        by = By.cssSelector(".btn");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).click();

        by = By.cssSelector("#tenantsTab > .label-tab");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).click();

        by = By.cssSelector("#userTab > .label-tab");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).click();

        by = By.cssSelector(".icon-logout-svg");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).click();

        by = By.id("logout-button");
        SeleniumUtils.waitClickable(driver, by);
        driver.findElement(by).click();

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
