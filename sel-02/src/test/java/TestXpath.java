import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestXpath {

    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    String testUrl = "https://qa-02.vizix.cloud/console/";
    String usernameCorrect = "testroot";
    String passwordCorrect = "Bcb123456!";

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(SeleniumUtils.IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }
    @Test
    public void test1() {
        driver.get(testUrl);
        driver.manage().window().setSize(new Dimension(1024, 768));

        driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys(usernameCorrect);

        driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(passwordCorrect);

        driver.findElement(By.xpath("//*[@id=\"login-wrap\"]/form/span[4]/button")).click();

        driver.findElement(By.xpath("//*[@id=\"tenantsTab\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"userTab\"]/span[2]")).click();

        driver.findElement(By.xpath("//*[@id=\"logo-icon-panel\"]/i")).click();

        driver.findElement(By.xpath("//*[@id=\"logout-button\"]")).click();

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
