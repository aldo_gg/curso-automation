package com.automation.example2;

public class CalcStatic {

    public CalcStatic() {
    }

    public int fact(int n) {
        int fac = 1;
        for (int i = 1; i < n; i++) {
            fac = StaticsMethods.mult(fac, i);
        }
        return fac;
    }
}
