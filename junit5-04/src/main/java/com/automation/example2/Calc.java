package com.automation.example2;

public class Calc {
    CalcCloud servicio = new CalcCloud();

    public Calc() {

    }

    public Calc(CalcCloud service) {
        this.servicio = service;
    }

    public int fact(int n) {
        int fac = 1;
        for (int i = 1; i <= n; i++) {
            fac = servicio.mult(fac, i);
        }
        return fac;
    }
}
