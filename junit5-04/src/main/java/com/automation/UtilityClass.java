package com.automation;

import java.io.*;
import java.math.BigDecimal;
import java.util.Arrays;

public class UtilityClass {


    public String readFile(String fileName) throws IOException {
        File file = new File(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        StringBuilder result = new StringBuilder("");
        String st;
        while ((st = br.readLine()) != null) {
            result.append(st+"\n");
        }
        br.close();
        return result.toString();
    }


}