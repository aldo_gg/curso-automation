package com.automation.example2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalcTest2 {

    @Mock
    CalcCloud serviceMocked;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callTest() {
        Mockito.when(serviceMocked.mult(1,1)).thenReturn(1);
        Mockito.when(serviceMocked.mult(1,2)).thenReturn(2);
        Mockito.when(serviceMocked.mult(2,3)).thenReturn(6);

        Calc calc = new Calc(serviceMocked);
        int actResult = calc.fact(3);
        int expResult = 6;
        assertEquals(expResult, actResult, "ERROR");

        Mockito.verify(serviceMocked).mult(1,1);
        Mockito.verify(serviceMocked).mult(1,2);
        Mockito.verify(serviceMocked).mult(2,3);

    }
}
