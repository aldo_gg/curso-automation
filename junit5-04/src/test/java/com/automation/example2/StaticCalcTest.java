package com.automation.example2;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * https://github.com/powermock/powermock/issues/906
 * PowerMock no soporta a Junit5
 */
//@RunWith(PowerMockRunner.class)
@PrepareForTest(StaticsMethods.class)
public class StaticCalcTest {

    @Test
    public void callTest() {
        PowerMockito.mockStatic(StaticsMethods.class);
        Mockito.when(StaticsMethods.mult(1,1)).thenReturn(1);
        Mockito.when(StaticsMethods.mult(1,2)).thenReturn(2);
        Mockito.when(StaticsMethods.mult(2,3)).thenReturn(6);

        CalcStatic calc = new CalcStatic();
        int actResult = calc.fact(3);
        int expResult = 6;
        assertEquals(expResult, actResult, "ERROR");
    }
}
