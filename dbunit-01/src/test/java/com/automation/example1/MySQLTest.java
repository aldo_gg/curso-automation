package com.automation.example1;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.database.search.TablesDependencyHelper;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLTest {


    public static void main(String[] args) throws Exception {

        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/riot_main", "root", "control123!");


        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        // full database export, this fails on mysql try to find sys_config table
        // IDataSet fullDataSet = connection.createDataSet();
        // FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));


        // partial database export
        QueryDataSet partialDataSet = new QueryDataSet(connection);
        partialDataSet.addTable("VIZ_APC_USER", "SELECT * FROM VIZ_APC_USER");
        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("partial_mysql.xml"));

        //dependent tables database export: export table X and all tables that
        //have a PK which is a FK on X, in the right order for insertion
        String[] depTableNames = TablesDependencyHelper.getAllDependentTables(connection, "VIZ_APC_USER");
        IDataSet depDataset = connection.createDataSet(depTableNames);
        FlatXmlDataSet.write(depDataset, new FileOutputStream("dependents_mysql.xml"));


    }
}
