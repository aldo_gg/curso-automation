package com.automation.example1;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DBUnitTest {

    private IDatabaseTester databaseTester;

    @BeforeEach
    public void setUp() throws Exception {

//Obtener instancia del DAO que testeamos
//pdao = new JDBCPeliculaDAO();

//Acceder a la base de datos
        databaseTester = new JdbcDatabaseTester("org.hsqldb.jdbcDriver",
                "jdbc:hsqldb:hsql://localhost/testdb", "sa", "");

//Inicializar el dataset en la BD
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        IDataSet dataSet = builder.build(
                this.getClass().getResourceAsStream("/partial.xml"));
        databaseTester.setDataSet(dataSet);

//Llamar a la operación por defecto setUpOperation
        databaseTester.onSetup();
    }

    @Test
    public void test() throws Exception {

        Connection conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/testdb", "SA", "");

        //Create statement
        Statement stmt = conn.createStatement();

        //Insert first reocod
        String record1="INSERT INTO users (ID, USERNAME, HASHED_PASSWORD) "
                + "VALUES (2,'Joseph','78A1A1DAE4E56FB0ABA67DADC5AB5AB808F505137EFEDECFB7C2F12A4195C58B')";
        stmt.executeUpdate(record1);

        Statement stmt3 = conn.createStatement();
        ResultSet rs3 = stmt3.executeQuery("SELECT COUNT(*) as total FROM USERS");
        rs3.next();
        int count = rs3.getInt("total");

        assertEquals(2, count, "Error numero de filas");
    }

    @AfterEach
    public void tearDown() throws Exception {
        databaseTester.onTearDown();
    }

}
