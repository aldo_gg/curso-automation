package com.automation.example1;

import com.automation.dao.UserDAO;
import com.automation.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

public class UserServiceMockitoTest {

    @Test
    public void testLogin() {
        UserService userService = Mockito.mock(UserService.class);
        Mockito.when(userService.findByUsername(eq("paola"))).thenReturn(null);
        Mockito.when(userService.login(any(String.class), any(String.class))).thenCallRealMethod();

        //login usuario no existe
        try {
            userService.login("paola", "paola");
            fail();
        } catch (Exception ex) {
            String mensajeVerdadero = ex.getMessage();
            String mensajeEsperado = "Invalid UserName";
            assertEquals(mensajeEsperado, mensajeVerdadero, "Error validacion usario");
        }
    }

    @Test
    public void testLoginCorrecto() {
        UserDAO userDAO = Mockito.mock(UserDAO.class);
        Mockito.when(userDAO.selectBy("username", "paola")).thenReturn(null);
        //login usuario no existe
        try {
            UserService userService = new UserService();
            userService.setUserDAO(userDAO);
            userService.login("paola", "paola");
            fail();
        } catch (Exception ex) {
            String mensajeVerdadero = ex.getMessage();
            String mensajeEsperado = "Invalid UserName";
            assertEquals(mensajeEsperado, mensajeVerdadero, "Error validacion usario");
        }
    }

}
