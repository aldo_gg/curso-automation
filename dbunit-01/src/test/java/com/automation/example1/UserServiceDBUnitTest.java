package com.automation.example1;

import com.automation.dao.HibernateSessionFactory;
import com.automation.dao.UserDAO;
import com.automation.entity.User;
import com.automation.service.UserService;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class UserServiceDBUnitTest {

    private IDatabaseTester databaseTester;

    @BeforeEach
    public void setUp() throws Exception {


//Acceder a la base de datos
        databaseTester = new JdbcDatabaseTester("org.hsqldb.jdbcDriver",
                "jdbc:hsqldb:hsql://localhost/testdb", "sa", "");

//Inicializar el dataset en la BD
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        IDataSet dataSet = builder.build(
                this.getClass().getResourceAsStream("/partial.xml"));
        databaseTester.setDataSet(dataSet);

//Llamar a la operación por defecto setUpOperation
        databaseTester.onSetup();
    }


    @Test
    public void testLogin(){
        Session session = HibernateSessionFactory.getInstance().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        try {
            UserDAO userDAO = new UserDAO();
            UserService userService = new UserService();
            userService.setUserDAO(userDAO);
            //login correcto
            userService.login("aldo", "aldo");
            //login usuario no existe
            try {
                userService.login("paola", "paola");
                fail();
            } catch (Exception ex) {
                String mensajeVerdadero = ex.getMessage();
                String mensajeEsperado = "Invalid UserName";
                assertEquals(mensajeEsperado, mensajeVerdadero, "Error validacion usario");
            }
            //login password invalido


            transaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            transaction.rollback();
            fail();
        }

    }

    @AfterEach
    public void tearDown() throws Exception {
        databaseTester.onTearDown();
    }

}
