create table USERS
(
    id bigint generated by default as identity (start with 1),
	USERNAME VARCHAR(50),
	HASHED_PASSWORD VARCHAR(256)
);