package com.automation.service;


import com.automation.dao.UserDAO;
import com.automation.entity.User;

import java.util.List;

public class UserService {

    private UserDAO userDAO;

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public UserService() {

    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User get(Long id) {
        User user = getUserDAO().selectById(id);
        return user;
    }


    public User insert(User user) {
        validateInsert(user);
        Long id = getUserDAO().insert(user);
        user.setId(id);
        return user;
    }

    public void validateInsert(User user) {

    }


    public User update(User user) {
        validateUpdate(user);
        getUserDAO().update(user);
        return user;
    }

    public void validateUpdate(User user) {

    }


    public void delete(User user) {
        validateDelete(user);
        getUserDAO().delete(user);
    }

    public void validateDelete(User user) {

    }

    public List<User> listAll() {
        return getUserDAO().selectAll();
    }

    public User findByUsername(String username) {
        return getUserDAO().selectBy("username", username);
    }

    public User findByHashpassword(String hashedPasword) {
        return getUserDAO().selectBy("hashedPasword", hashedPasword);
    }

    public int login(String username, String password) {
        User user = findByUsername(username);
        if (user == null) {
            throw new RuntimeException("Invalid UserName");
        }
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
        if (user.getHashedPassword().equalsIgnoreCase(sha256hex)) {
            return 0;
        } else {
            throw new RuntimeException("Invalid Password");
        }
    }

}
