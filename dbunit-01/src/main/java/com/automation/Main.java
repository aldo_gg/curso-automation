package com.automation;

import com.automation.dao.HibernateSessionFactory;
import com.automation.dao.UserDAO;
import com.automation.entity.User;
import com.automation.service.UserService;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class Main {

    public static void main(String[] args )  {

        Session session = HibernateSessionFactory.getInstance().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        try {
            UserDAO userDAO = new UserDAO();
            UserService userService = new UserService();
            userService.setUserDAO(userDAO);
            List<User> usersList = userService.listAll();
            for (User user : usersList) {
                System.out.println(user.getId()+","+user.getUsername()+","+user.getHashedPassword());
            }
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
        }
    }

}
