package com.automation.dao;

import com.automation.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import java.util.List;

public class UserDAO {
    private SessionFactory sessionFactory = HibernateSessionFactory.getInstance();

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public User selectById(Long id) {
        return (User) getSession().get(User.class, id);
    }

    public User selectBy(String property, Object value) {
        Criteria c1 = getSession().createCriteria(User.class);
        c1.add(Restrictions.eq(property, value));
        c1.setCacheable(true);
        return (User) c1.uniqueResult();
    }

    public Long insert(User object) {
        Session session = getSession();
        session.save(object);
        Long result = (Long) session.getIdentifier(object);
        session.flush();
        object.setId(result);
        return result;
    }

    public void update(User object) {
        Session session = getSession();
        session.update(object);
        session.flush();
    }

    public void delete(User object) {
        Session session = getSession();
        session.delete(object);
        session.flush();
    }

    public List<User> selectAll() {
        Query query = getSession().createQuery("from " + User.class.getName());
        return (List<User>) query.list();
    }
}
