package com.automation.dao;


import com.automation.exception.UserException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.context.internal.ThreadLocalSessionContext;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.hibernate.service.ServiceRegistry;


import java.lang.InstantiationException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/**
 * Created by agutierrez on 21-04-14.
 */
// TODO: AGG Rename to HibernateUtils
public class HibernateSessionFactory {
    private static final Logger logger = LogManager.getLogger( HibernateSessionFactory.class );

    private volatile static SessionFactory INSTANCE;

    // only test
    public static boolean instanceTest = Boolean.FALSE;
    public static final int MAX_CONNECTIONS_RETRY = 4;

    public static SessionFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (HibernateSessionFactory.class) {
                if (INSTANCE == null) {
                    boolean retry = true;
                    do{
                        Configuration configuration = new Configuration();
                        configuration.configure();

                        Properties properties = configuration.getProperties();
                            properties.put( "hibernate.cache.use_second_level_cache", "false" );
                            properties.put( "hibernate.cache.use_query_cache", "false" );

                        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                                .configure()
                                .applySettings(properties)
                                .build();
                        try {
                            INSTANCE = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
                            retry = false;
                        }catch (HibernateException e){
                            e.printStackTrace();
                            logger.warn("Startup process for [Services] waiting for retry", e);
                        }
                    }while (retry);

                }
            }
        }
        return INSTANCE;
    }


    /**
     * This method was deprecated on 2016/09/27
     * deprecated version: 4.3.0_RC13
     */
    @Deprecated
    public static synchronized void restartInstance() {
        INSTANCE.close();
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        INSTANCE = configuration.buildSessionFactory(serviceRegistry);
    }

    public static synchronized void closeInstance() {
        if (INSTANCE != null) {
            INSTANCE.close();
        }
        if (INSTANCE!=null){
            INSTANCE.close();
        }
    }

    public static synchronized void setInstance(SessionFactory newInstance) {
        if (INSTANCE != null) {
            INSTANCE.close();
        }
        INSTANCE = newInstance;
    }

    public static synchronized void setInstanceForTest(SessionFactory newInstance) {
        if (INSTANCE == null) {
            INSTANCE = newInstance;
            instanceTest = Boolean.TRUE;
        }
    }

    public static void openTransaction() {
        int connectionRetry = 0;
        boolean connected = false;
        do {
            Session session = null;
            try{
                session = getInstance().getCurrentSession();
                session.setFlushMode(FlushMode.COMMIT);
                Transaction transaction = session.getTransaction();
                if(transaction.getStatus() != TransactionStatus.ACTIVE){
                    transaction.begin();
                }
//                executeTransactionVerification();
                connected = true;
            } catch (Exception e) {
                wipeSessionFromThread(session, e);
                logger.error("Unable to get an available database connection. " +
                        "Retry " + (connectionRetry + 1) + " of " + MAX_CONNECTIONS_RETRY, e);
                connectionRetry++;
            }

        } while (connectionRetry < MAX_CONNECTIONS_RETRY && !connected);
        if (connectionRetry >= MAX_CONNECTIONS_RETRY) {
            throw new UserException("Unable to get an available database connection after"
                    + MAX_CONNECTIONS_RETRY + " retry maximum. Aborting Connection");
        }
    }

//    public static void executeTransactionVerification() throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
//        Class<?> clazz = Class.forName("com.tierconnect.riot.iot.services.DataTypeService");
//        Object dataType = clazz.newInstance();
//        clazz.getMethod("get", Long.class).invoke(dataType, System.currentTimeMillis());
//        logger.debug("Transaction was verified");
//    }

    public static void wipeSessionFromThread(Session session, Exception e) {
        if (e.getCause() != null && e.getCause().getMessage().contains("Connection is closed")) {
            ThreadLocalSessionContext.unbind(getInstance());
        }
        if (session != null) {
            session.disconnect();
        }
    }

    public static void commitTransaction() {
        Session session = getInstance().getCurrentSession();
        Transaction transaction = session.getTransaction();
        if(transaction.getStatus() == TransactionStatus.ACTIVE){
            transaction.commit();
        }
    }

    public static void closeSession() {
        Session session = getInstance().getCurrentSession();
        session.close();
    }


    public static void rollbackTransaction(){
        Session session = getInstance().getCurrentSession();
        Transaction transaction = session.getTransaction();
        if(transaction.getStatus() == TransactionStatus.ACTIVE){
            try{
                transaction.rollback();
            } catch (HibernateException e) {
                logger.warn("Could not execute transaction rollback", e);
            }
        }
    }

}
