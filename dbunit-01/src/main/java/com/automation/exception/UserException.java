package com.automation.exception;

public class UserException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final int USER_ERROR_STATUS = 400;
    protected int status = 400;

    public UserException() {
    }

    public UserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, int status) {
        super(message);
        this.status = status;
    }

    public UserException(String message, Throwable cause, int status) {
        super(message, cause);
        this.status = status;
    }


    public UserException(Throwable cause) {
        super(cause);
    }

    public String getMessage() {
        return super.getMessage();
    }

    public int getStatus() {
        return this.status;
    }
}
