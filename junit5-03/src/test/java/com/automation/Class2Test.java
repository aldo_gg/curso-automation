package com.automation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Pares")
public class Class2Test {
    Class2 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class2();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}
