package com.automation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Pares")
public class Class6Test {
    Class6 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class6();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}
