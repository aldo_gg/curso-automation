package com.automation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Impares")
public class Class5Test {
    Class5 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class5();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}