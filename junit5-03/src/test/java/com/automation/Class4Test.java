package com.automation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Pares")
public class Class4Test {
    Class4 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class4();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}
