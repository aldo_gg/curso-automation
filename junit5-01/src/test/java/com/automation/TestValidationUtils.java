package com.automation;

import org.junit.jupiter.api.*;

@Tag("test1")
public class TestValidationUtils {

    ValidationUtils validationUtils;

    @BeforeEach
    void init() {
        System.out.println("@BeforeEach");
        validationUtils = new ValidationUtils();
        validationUtils.init();
    }

    @Test
    void testValidateEmail() {
        Assertions.assertTrue(validationUtils.validateEmail("aldo.agg@gmail.com"), "aldo.agg@gmail is an invalid email");
        Assertions.assertFalse(validationUtils.validateEmail(null), "null is a valid email");
    }

    @AfterEach
    void tearDown() {
        validationUtils.end();
        System.out.println("@AfterEach");
    }

}
