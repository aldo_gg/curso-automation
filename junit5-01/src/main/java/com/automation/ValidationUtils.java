package com.automation;

public class ValidationUtils {

    public void init() {

    }

    public boolean validateEmail(String email) {
        if (email == null) {
            return false;
        }
        return email.contains("@");
    }

    public void end() {

    }

}
