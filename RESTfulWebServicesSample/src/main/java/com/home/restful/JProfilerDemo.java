package com.home.restful;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JProfilerDemo {

    public static List<String> listString = new ArrayList();
    public static List<Thread> listThreads = new ArrayList();


    public static void slowMethod() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void slowMethodCpuIntensive() {
        double sum =0;
        for (int i=0; i < 4000000; i++) {
            sum = sum + Math.sin(i);
        }
    }

    public static void progresivelySlowMethod() {
        int countA = 0;
        for (String s: listString) {
            if (s.contains("A")) {
                countA++;
            }
        }
    }

    public static void memoryHungryNoReturnMethod() {
        for (int i=0; i < 100; i++) {
            char[] data = new char[1000000];
            String str = new String(data);
            listString.add(str);
        }
    }

    public static void memoryHungryReturnMethod() {
        List list = new ArrayList();
        for (int i=0; i < 100; i++) {
            char[] data = new char[1000000];
            String str = new String(data);
            list.add(str);
        }
    }

    public static void memoryBomb() {
        for (int i=0; i < 100000000; i++) {
            char[] data = new char[1000000];
            String str = new String(data);
            listString.add(str);
        }
    }

    public static void threadIncreaseMethod() {
        for (int i=0; i< 50; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    int b = 2 + 2;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
            listThreads.add(t);
        }
        return;
    }

    public static void main (String[] args) {
        memoryHungryReturnMethod();
    }

}
