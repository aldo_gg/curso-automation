package com.automation;

import java.util.*;

public class MapComparator {

    public static boolean equalMaps(Map map1, Map map2) {
        if (map1 == map2)  {
            return true;
        }
        Map aux1 = sortMap(map1);
        Map aux2 = sortMap(map2);

        if (aux1.toString().equals(aux2.toString())) {
            return true;
        } else {
            return false;
        }
    }

    private static Map sortMap(Map map1) {
        Map aux1 = new LinkedHashMap();
        List keys1 = new ArrayList(map1.keySet());
        Collections.sort(keys1);
        for (Object key : keys1) {
            Object value = map1.get(key);
            aux1.put(key, value instanceof Map ? sortMap((Map) value): value);
        }
        return aux1;
    }
}
