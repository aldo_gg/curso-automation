package com.automation;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MapComparatorTest {

    @Test
    public void testMapComparator() {
        Map<Integer, String> wordsByKey = new HashMap<>();
        wordsByKey.put(1, "one");
        wordsByKey.put(2, "two");
        wordsByKey.put(3, "three");
        wordsByKey.put(4, "four");
        assertTrue(MapComparator.equalMaps(wordsByKey, wordsByKey));
        Map<Integer, String> wordsByKey2 = new HashMap<>();
        wordsByKey2.put(1, "one");
        wordsByKey2.put(2, "two");
        wordsByKey2.put(3, "three");
        wordsByKey2.put(4, "four");
        assertTrue(MapComparator.equalMaps(wordsByKey, wordsByKey2));

        assertTrue(MapComparator.equalMaps(new HashMap(), new HashMap()));

        assertTrue(MapComparator.equalMaps(new HashMap(), new ConcurrentHashMap()));

        Map<Integer, String> wordsByKey3 = new HashMap<>();
        wordsByKey3.put(1, "one");
        wordsByKey3.put(2, "two");
        wordsByKey3.put(3, "three");
        assertFalse(MapComparator.equalMaps(wordsByKey, wordsByKey3));

        Map<Integer, String> wordsByKey4 = new LinkedHashMap<>();
        wordsByKey4.put(4, "four");
        wordsByKey4.put(3, "three");
        wordsByKey4.put(2, "two");
        wordsByKey4.put(1, "one");
        assertTrue(MapComparator.equalMaps(wordsByKey, wordsByKey4));
    }

    @Test
    public void testMapComparator2() {
        Map<String, Object> wordsByKey = new HashMap<>();
        wordsByKey.put("1", "one");
        Map<String, Object> wordsByKeyAux = new HashMap<>();
        wordsByKeyAux.put("51", "one");
        wordsByKeyAux.put("52", "two");
        wordsByKey.put("5", wordsByKeyAux);

        Map<String, Object> wordsByKey2 = new HashMap<>();
        Map<String, Object> wordsByKeyAux2 = new LinkedHashMap<>();
        wordsByKeyAux2.put("52", "two");
        wordsByKeyAux2.put("51", "one");
        wordsByKey2.put("5", wordsByKeyAux2);
        wordsByKey2.put("1", "one");

        assertTrue(MapComparator.equalMaps(wordsByKey, wordsByKey2));

        wordsByKeyAux2.remove("51");

        assertFalse(MapComparator.equalMaps(wordsByKey, wordsByKey2));
    }


}
