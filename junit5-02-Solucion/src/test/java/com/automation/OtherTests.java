package com.automation;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

@Tag("other")
public class OtherTests {

    @Timeout(5)
    @Test
    void testTimeOut() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @RepeatedTest(10)
    void repeatedTest() {
        System.out.println("repeatedTest");
    }
}
