package com.automation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.time.Month;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorParameterizedTest {

    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    public static int[][] multiplyData() {
        return new int[][] {
                { 1 , 2, 2 },
                { 5, 3, 15 },
                { 121, 4, 484 } };
    }

    @ParameterizedTest
    @MethodSource("multiplyData")
    void testMultiplyWithParameters(int[] data) {
        int m1 = data[0];
        int m2 = data[1];
        int expected = data[2];
        assertEquals(expected, calculator.multiply(m1, m2));
    }

    @ParameterizedTest
    @EnumSource(value = Month.class, mode = EnumSource.Mode.EXCLUDE, names = {"FEBRUARY", "APRIL", "JUNE", "SEPTEMBER", "NOVEMBER"})
    void testM(Month month) {
        assertTrue(calculator.is31DayMonth(month));
    }

    @ParameterizedTest
    @ValueSource(ints = { 1, 3, 5, 7,8,10,12 })
    void testWithValueSource(int argument) {
        assertTrue(calculator.is31DayMonth(Month.of(argument)));
    }

    @ParameterizedTest
    @ValueSource(ints = { 2, 4, 6, 11 })
    void testWithValueSource2(int argument) {
        assertFalse(calculator.is31DayMonth(Month.of(argument)));
    }

    @ParameterizedTest
    @MethodSource("stringIntAndListProvider")
    void testMultiplyWithStream(int m1, int m2, int expected) {
        assertEquals(expected, calculator.multiply(m1, m2));
    }

    static Stream<Arguments> stringIntAndListProvider() {
        return Stream.of(
                arguments(7, 7, 49),
                arguments(8, 8, 64),
                arguments(1, 0, 0)
        );
    }

    @ParameterizedTest
    @CsvSource({
            "11,11,121",
            "11,10,110",
            "5,5,25",
    })
    void testWithCsvSource(String m1s, String m2s, String expectedS) {
        int m1 = Integer.valueOf(m1s);
        int m2 = Integer.valueOf(m2s);
        int expected = Integer.valueOf(expectedS);
        assertEquals(expected, calculator.multiply(m1, m2));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/add.csv", numLinesToSkip = 1)
    void testWithCsvFileSource(int m1, int m2, int expected) {
        assertEquals(expected, calculator.add(m1, m2));
    }

}
