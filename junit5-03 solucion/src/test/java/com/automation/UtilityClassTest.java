package com.automation;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UtilityClassTest {

    UtilityClass utilityClass;

    @BeforeEach
    public void beforeEach() {
        utilityClass = new UtilityClass();
    }

    @Test
    public void testCompareStrings(){
        // cadenas null
        assertTrue(utilityClass.smartCompareStrings(null, null));
        // cadenas vacias
        assertTrue(utilityClass.smartCompareStrings("", ""));
        assertFalse(utilityClass.smartCompareStrings("", null));
        assertFalse(utilityClass.smartCompareStrings(null, ""));

        // comparar con si mismo
        String sa = "a";
        assertTrue(utilityClass.smartCompareStrings(sa, sa));
        // mismo tamnio distinto contenido
        String sb = "b";
        assertFalse(utilityClass.smartCompareStrings(sa, sb));
        // mismo contenido con espacios
        String cadena1 = " a ";
        String cadena2 = "            a ";
        assertTrue(utilityClass.smartCompareStrings(cadena1, cadena2));
        // mayusculas contra minusculas
        String cadena100 = "Hola Mundo";
        String cadena200 = "HOLA MUNDO";
        assertTrue(utilityClass.smartCompareStrings(cadena100, cadena200));

    }

    @Test
    public void testCompareStringVsDouble(){
        // cadenas null vs double null
        assertTrue(utilityClass.compareStringVsDouble(null, null));
        // cadenas vacia vs double null o no null
        assertEquals(false, utilityClass.compareStringVsDouble("", null));
        assertEquals(false, utilityClass.compareStringVsDouble("", 5.0));

        // cadenascon un numero vs el mismo numero como double
        assertEquals(true, utilityClass.compareStringVsDouble("5.0", 5.0));

    }

}

