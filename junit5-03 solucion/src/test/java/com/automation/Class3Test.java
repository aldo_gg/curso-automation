package com.automation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Impares")
public class Class3Test {
    Class3 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class3();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}

