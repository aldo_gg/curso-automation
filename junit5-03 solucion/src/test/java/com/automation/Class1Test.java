package com.automation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

@Tag("Impares")
public class Class1Test {
    Class1 objeto;

    @BeforeEach
    public void beforeEach() {
        objeto = new Class1();
    }

    @Test
    public void methodTest() {
        objeto.method();
    }
}
