package com.automation;

import java.io.*;
import java.math.BigDecimal;
import java.util.Arrays;

public class UtilityClass {


    final static int serialVersionUID = 10;

    public boolean smartCompareStrings(String a, String b) {
        if (a ==null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        return a.trim().toUpperCase().equals(b.trim().toUpperCase());
    }


    public boolean compareStringVsDouble(String a, Double b) {
        if (a ==null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        return a.trim().toUpperCase().equals(b.toString().trim().toUpperCase());
    }

    public String toUpperCase(String a) {
        if (a == null) {
            return null;
        } else {
            return a.toUpperCase();
        }
    }

    public boolean compare(int[] a, int[] b) {
        return Arrays.equals(a, b);
    }

    public void log1() {
        System.out.format("%s %n", "Hello world");
    }

    public void log2() {
        System.out.format("%s %n", "Hello world 2");
    }

    public void testIntegers() {
        Integer a = 1;
        Integer b = 1;
        System.out.println((int)a == (int)b);
    }

    public void testBooleans() {
        Boolean a = false;
        Boolean b =  Boolean.FALSE;
        System.out.println(a.equals(b));
    }

    public String readFile(String fileName) throws IOException {
        File file = new File(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        StringBuilder result = new StringBuilder("");
        String st;
        while ((st = br.readLine()) != null) {
            result.append(st);
        }
        br.close();
        return result.toString();
    }

    public boolean areEqual(boolean a1, boolean a2, boolean a3, boolean a4, boolean a6, boolean a7, boolean a8, boolean a9, boolean a10) {
        return a1 == a2 == a3 == a4 == a6 == a7 == a8 == a9 == a10;
    }


    public boolean bigDecimalTest() {
        BigDecimal b = new BigDecimal("0.1");
        double c = 0.1;
        return Math.abs(b.doubleValue() - c) < .0000001;
    }


}