package com.automation.api;

import com.automation.http.HTTPResponse;
import com.automation.http.HTTPTester;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RestExampleAppTest {
    String testUrl = "http://localhost:8080/RESTfulWebServicesSample/rest";
    HTTPTester httpTester;

    @BeforeEach
    public void setUp() throws Exception {
        httpTester = new HTTPTester(new URL(testUrl));
    }

    @Test
    public void testUser() throws IOException {
        HTTPResponse httpResponse;


        //aumentar headers basicos
        httpTester = new HTTPTester(new URL(testUrl));

        httpTester.addHeader("Accept", "application/json");
        httpTester.addHeader("Content-Type", "application/json");

        //adicionar el header de token

        //crear una persona
        Map<String, Object> inputJson = new HashMap<>();
        inputJson = new HashMap<>();
        inputJson.put("firstName", "pepito");
        inputJson.put("lastName", "perez");
        httpResponse = httpTester.postJson("/persons", inputJson);
        assertTrue(httpResponse.getCode() == 201);
        Map<String, Object> response = httpResponse.getJsonBodyAsMap();
        assertTrue(response.get("firstName") != null);
        assertTrue(response.get("lastName") != null);
        List links = (List) response.get("links");
        assertTrue(links != null);
        assertTrue(links.size() > 0);
        assertTrue(response.get("joinedDate") != null);

    }

    @AfterEach
    public void tearDown() throws Exception {
        httpTester.close();
    }

}
