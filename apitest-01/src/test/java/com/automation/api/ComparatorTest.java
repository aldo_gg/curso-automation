package com.automation.api;

import com.automation.http.JSONUtils;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComparatorTest {

    public String readAsString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString("UTF-8");
    }

    @Test
    public void test() throws IOException {
        String actual = readAsString(ComparatorTest.class.getResourceAsStream("/actualInsertResponse.json"));
        String expected = readAsString(ComparatorTest.class.getResourceAsStream("/expectedInsertResponse.json"));
        assertTrue(JSONUtils.similarJson(expected, actual));

    }
}
