package com.automation.api;

import com.automation.http.HTTPResponse;
import com.automation.http.HTTPTester;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GoogleTest {

    @Test
    public void testUser() throws IOException {
        HTTPTester httpTester;
        HTTPResponse httpResponse;


        httpTester = new HTTPTester(new URL("https://www.google.com"));
        httpResponse = httpTester.get("/");
        assertEquals(200, httpResponse.getCode());

    }
}
