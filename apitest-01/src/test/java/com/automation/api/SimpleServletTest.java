package com.automation.api;

import com.automation.http.HTTPResponse;
import com.automation.http.HTTPTester;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimpleServletTest {

    String testUrl = "http://localhost:8080/SimpleServlet";
    HTTPTester httpTester;

    @BeforeEach
    public void setUp() throws Exception {
        httpTester = new HTTPTester(new URL(testUrl));
    }

    @Test
    public void testLoginWithCookie() throws IOException {

        HTTPResponse httpResponse;
        Map<String, Object> inputForm;
        inputForm = new HashMap<>();
        inputForm.put("name", "pepito");
        inputForm.put("password", "admin123");
        httpResponse = httpTester.postForm("/LoginServlet", inputForm);
        assertTrue(httpResponse.getCode() == 200);

        httpResponse = httpTester.get("/ProfileServlet");
        assertTrue(httpResponse.getCode() == 200);

    }

    @AfterEach
    public void tearDown() throws Exception {
        httpTester.close();
    }

}
