package com.automation.api;

import com.automation.http.HTTPResponse;
import com.automation.http.HTTPTester;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MojixUserAPITest {
    String testUrl = "https://qa-02.vizix.cloud/riot-core-services/api";
    String usernameCorrect = "testroot";
    String passwordCorrect = "Bcb123456!";

    HTTPTester httpTester;

    @BeforeEach
    public void setUp() throws Exception {
        httpTester = new HTTPTester(new URL(testUrl));
    }

    @Test
    public void testUser() throws IOException {
        HTTPTester httpTester;
        HTTPResponse httpResponse;


        //aumentar headers basicos
        httpTester = new HTTPTester(new URL(testUrl));

        httpTester.addHeader("Accept", "application/json");
        httpTester.addHeader("Content-Type", "application/json");
        httpTester.addHeader("ClientID", "3823393308");

        //logear al usuario
        Map<String, Object> inputJson = new HashMap<>();
        inputJson.put("username", usernameCorrect);
        inputJson.put("password", passwordCorrect);

        httpResponse = httpTester.postJson("/user/login", inputJson);
        assertEquals(200, httpResponse.getCode());
        Map<String, Object> jsonResponse = httpResponse.getJsonBodyAsMap();
        assertEquals(usernameCorrect, jsonResponse.get("username"));

        //adicionar el header de token
        String token = (String) jsonResponse.get("token");
        assertTrue(StringUtils.isNotEmpty(token));
        httpTester.addHeader("token", token);

        //crear un usuario
        Long groupId = ((Number) ((Map) jsonResponse.get("group")).get("id")).longValue();
        String username = "test" + UUID.randomUUID();
        inputJson = new HashMap<>();
        inputJson.put("username", username);
        inputJson.put("password", "AdifficultPassword123!");
        inputJson.put("group.id", groupId);
        httpResponse = httpTester.putJson("/user/", inputJson);
        assertEquals(201, httpResponse.getCode());
        jsonResponse = httpResponse.getJsonBodyAsMap();
        Long newUserId = ((Number) jsonResponse.get("id")).longValue();

        //borrar un usuario
        httpTester.delete("/user/" + newUserId);
        assertEquals(201, httpResponse.getCode());

        //listar todos los usuarios
        httpResponse = httpTester.get("/user/");
        assertEquals(200, httpResponse.getCode());
        jsonResponse = httpResponse.getJsonBodyAsMap();
        List<Map<String, Object>> users = (List<Map<String, Object>>) jsonResponse.get("results");
        for (Map<String, Object> user : users) {
            System.out.println(user.get("username"));
        }
    }

    @AfterEach
    public void tearDown() throws Exception {
        httpTester.close();
    }

}
