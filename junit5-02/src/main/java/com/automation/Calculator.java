package com.automation;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;

public class Calculator {

    public int multiply(int i, int j) {
        return  i * j;
    }

    public int add(int i, int j) {
        return  i + j;
    }


    public boolean is31DayMonth(Month month) {
        if (month == Month.JANUARY || month == Month.MARCH || month == Month.MAY || month == Month.JULY || month == Month.AUGUST || month == Month.OCTOBER || month == Month.DECEMBER) {
            return true;
        } else {
            return false;
        }
    }

}
