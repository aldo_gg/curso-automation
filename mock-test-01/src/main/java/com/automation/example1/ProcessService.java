package com.automation.example1;

import com.automation.UtilityClass;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class ProcessService {

    FTPService ftpService = null;
    UtilityClass utilityClass = new UtilityClass();


    public ProcessService() {
    }

    public ProcessService(FTPService ftpService) {
        this.ftpService = ftpService;
    }

    public void setFtpService(FTPService ftpService) {
        this.ftpService = ftpService;
    }

    public void processFile(File file) throws IOException {

        //OBTENER DEL FILE LA LISTA DE RECORDS
        List<Record> records = csvToListRecords(utilityClass.readFile(file.getAbsolutePath()));

        //Aggregar/Juntar/Procesar la lista de records en una nueva lista de records
        List<Record> recordsSumados = aggregate(records);////


        for (Record record: recordsSumados) {
            //save recordsAggrefatod to file
            String string = listRecordsToCSV(Arrays.asList(record));
            ftpService.loadFile(string, record.getCi()+"-"+record.getFechaNacimiento()+".csv");
        }
    }

    protected String listRecordsToCSV(List<Record> asList) {
        StringBuilder sb = new StringBuilder();
        sb.append("CI,NOMBRE,APELLIDOS,FECHA_NACIMIENTO,MONTO\n");
        for (Record record : asList) {
          sb.append(record.getCi()+","+record.getNombre()+","+record.getApellidos()+","+record.getFechaNacimiento()+","+record.getMonto()+"\n");
        }
        return sb.toString();
    }

    protected List<Record> aggregate(List<Record> records) {
        Map<String, Record> resultMap = new LinkedHashMap<>();
        for (Record record : records) {
            String key = record.getCi()+"_"+record.getFechaNacimiento();
            if (!resultMap.containsKey(key)) {
                resultMap.put(key, record);
            } else {
                Record oldRecord = resultMap.get(key);
                oldRecord.setMonto(oldRecord.getMonto().add(record.getMonto()));
            }
        }
        return new ArrayList<>(resultMap.values());
    }

    protected List<Record> csvToListRecords(String text) {
        String[] lines = text.split("\\r?\\n");
        List<Record> records = new ArrayList<>();
        int i=0;
        for (String line : lines) {
            if (i!= 0) {
                String[] parts = line.split(",");
                Record record = new Record();
                record.setCi(cleanString(parts[0]));
                record.setNombre(cleanString(parts[1]));
                record.setApellidos(cleanString(parts[2]));
                record.setFechaNacimiento(cleanString(parts[3]));
                record.setMonto(new BigDecimal(cleanString(parts[4])));
                records.add(record);
            }
            i++;
        }
        return records;
    }

    protected String cleanString(String a) {
        if (a==null) {
            return a;
        } else {
            return a.trim();
        }
    }



}
