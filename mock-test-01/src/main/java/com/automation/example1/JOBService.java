package com.automation.example1;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JOBService {
    FileService fileService;
    ProcessService processService;

    public JOBService() {

    }

    public void run () {
        FileService fileService = new FileService();
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (fileService.hasFiles("*.csv")) {
                List<File> files = fileService.getFiles("*.csv");
                for (File file : files) {
                    try {
                        processService.processFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
