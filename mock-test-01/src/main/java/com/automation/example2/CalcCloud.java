package com.automation.example2;

public class CalcCloud {

    public CalcCloud() {

    }

    public int mult(int a, int b) {
        System.out.println("Llamada real mult "+a+" * "+b);
        return a * b;
    }

    public String convertirAMayusculas(String abc) {
        return abc.trim().toUpperCase();
    }
}
