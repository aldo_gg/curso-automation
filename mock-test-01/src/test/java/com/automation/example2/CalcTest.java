package com.automation.example2;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class CalcTest {

    CalcCloud serviceMocked = Mockito.mock(CalcCloud.class);

    @Test
    public void callTest() {
        when(serviceMocked.mult(1,1)).thenReturn(1);
        when(serviceMocked.mult(1,2)).thenReturn(2);
        when(serviceMocked.mult(2,3)).thenReturn(6);

        Calc calc = new Calc(serviceMocked);
        int actResult = calc.fact(3);
        int expResult = 6;
        assertEquals(expResult, actResult, "ERROR");

        Mockito.when(serviceMocked.mult(6,4)).thenReturn(24);

        actResult = calc.fact(4);
        expResult = 24;
        assertEquals(expResult, actResult, "ERROR");

    }

}
