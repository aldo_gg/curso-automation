package com.automation.example2;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.AdditionalMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class MockitoTest {

    CalcCloud serviceMocked = Mockito.mock(CalcCloud.class);

    @Test
    public void testDemo() {


        Mockito.when(serviceMocked.convertirAMayusculas(isNull(String.class))).thenThrow(RuntimeException.class);
        try {
            String s1 = serviceMocked.convertirAMayusculas("null");
            String s2 = serviceMocked.convertirAMayusculas(null);
            fail();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        when(serviceMocked.mult(1,1)).thenReturn(1);
        when(serviceMocked.mult(1,2)).thenReturn(2);
        when(serviceMocked.mult(2,3)).thenReturn(6);

        Calc calc = new Calc(serviceMocked);
        int actResult = calc.fact(3);
        int expResult = 6;
        assertEquals(expResult, actResult, "ERROR");
        when(serviceMocked.mult(and(gt(0), lt(2)), gt(1))).thenThrow(RuntimeException.class);
        try {
            actResult = calc.fact(1);
            actResult = calc.fact(4);
            fail();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        doThrow(RuntimeException.class).when(serviceMocked).convertirAMayusculas(eq("cde"));
        try {
            String s1 = serviceMocked.convertirAMayusculas("abc");
            String s2 = serviceMocked.convertirAMayusculas("cde");
            fail();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}
