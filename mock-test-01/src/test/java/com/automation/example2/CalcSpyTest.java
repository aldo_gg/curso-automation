package com.automation.example2;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalcSpyTest {

    CalcCloud serviceMocked = Mockito.spy(CalcCloud.class);

    @Test
    public void callTest() {
        Mockito.when(serviceMocked.mult(1,1)).thenReturn(1);
        Mockito.when(serviceMocked.mult(1,2)).thenReturn(2);
        Mockito.when(serviceMocked.mult(2,3)).thenReturn(6);

        Calc calc = new Calc(serviceMocked);
        int actResult = calc.fact(3);
        int expResult = 6;
        assertEquals(expResult, actResult, "ERROR");


        actResult = calc.fact(4);
        expResult = 24;
        assertEquals(expResult, actResult, "ERROR");
    }
}
