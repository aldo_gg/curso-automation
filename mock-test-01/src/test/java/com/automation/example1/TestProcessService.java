package com.automation.example1;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class TestProcessService {

    @BeforeEach
    public void beforeEach() {

    }

    @Test
    public void testProcessFile() {
        FTPService ftpServiceMocked = Mockito.mock(FTPService.class);
        ProcessService processService = new ProcessService(ftpServiceMocked);
        try {
            File file = new File(getClass().getClassLoader().getResource("input.csv").getFile());
            processService.processFile(file);
        } catch (IOException e) {
            Assertions.fail(e);
        }
        try {
            verify(ftpServiceMocked, times(3)).loadFile(any(String.class), any(String.class));
        } catch (IOException e) {
            Assertions.fail(e);
        }

        try {
            verify(ftpServiceMocked, times(1)).loadFile(any(String.class), eq("1234567-1980-01-01.csv"));
        } catch (IOException e) {
            Assertions.fail(e);
        }

        try {
            verify(ftpServiceMocked, atLeastOnce()).loadFile(any(String.class), eq("1234567-1980-01-01.csv"));
        } catch (IOException e) {
            Assertions.fail(e);
        }

        try {
            verify(ftpServiceMocked, never()).loadFile(any(String.class), eq("error.csv"));
        } catch (IOException e) {
            Assertions.fail(e);
        }

    }


    @Test
    public void  listRecordsToCSVTest() {
        //Do someTesting
    }

    @Test
    private void aggregateTest() {
        //Do someTesting
    }

    @Test
    public void csvToListRecordsTest() {
        ProcessService processService = new ProcessService();
        String input = "CI,NOMBRE,APELLIDOS,FECHA_NACIMIENTO,MONTO\n" +
                "1234567, JUAN, PEREZ, 1980-01-01, 500";
        List<Record> recordList = processService.csvToListRecords(input);
        assertEquals(1, recordList.size(), "Error tamanio lista");
        Record record = recordList.get(0);
        assertEquals("1980-01-01", record.getFechaNacimiento(), "error fecga");

        //Do someTesting
    }

    @AfterEach
    public void afterEach() {

    }
}
